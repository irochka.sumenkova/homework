"""

Даны два целых числа. Выведите значение наименьшего из них.
"""
user_num1 = int(input("введите число 1"))
user_num2 = int(input("введите число 2"))

if user_num1 < user_num2:
    print("minimal=",user_num1)
else:
    print("minimal=",user_num2)

"""
Если вводится температура в градусах по шкале Цельсия, то она переводится в температуру по шкале Фаренгейта.
        Или наоборот: температура по Фаренгейту переводится в температуру по Цельсию.
"""
temp_cels = float(input("введите температуру по цельсию"))
if temp_cels is not None:
    kalvin_temp = temp_cels + 273.15
    print("kalvin_temp=", kalvin_temp)

"""
Из списка случайных чисел,определить и вывести на экран нечетные числа.
"""
user_list = (56, 49, 84, 653, 451, 654654, 44)
for i in user_list:
    if i % 2 == 1:
        print(i)

"""
Вводится целое число, обозначающее код символа по таблице ASCII. Определить, это код английской буквы или
        какой-либо иной символ.
"""
user_num = int(input("введите код буквы"))

if ord("a") < user_num < ord("z") or ord("A") < user_num < ord("Z"):
    print("это буква английского алфавита")
else:
    print("это другой символ")
"""
Вводятся два целых числа. Проверить делится ли первое на второе. Вывести на экран сообщение об этом, а также
        остаток (если он есть) и частное (в любом случае).
"""
user_num1 = int(input("введите первое число"))
user_num2 = int(input("введите второе число"))
result = int(user_num1 / user_num2)
surpluse = user_num1 % user_num2

if user_num2 != 0:
    print(user_num1, "/", user_num2, "=", result)
    if surpluse != 0:
        print("остача=", surpluse)
else:
    print("на 0 делить нельзя!")
"""
 Поочередно вводится 5 цифр, вывести их сумму(успользуя for)
"""
result = 0
for i in range(5):
    i = int(input("введите число"))
    result += i
print("result=", result)
"""
Даны два целых числа A и В. Выведите все числа от A до B включительно, в порядке возрастания, если A < B, или в
        порядке убывания в противном случае.
   
"""
a = int(input("введите целое число"))
b = int(input("введите целое число"))
if a < b:
    for i in range(a, b + 1):
        print(i)
else:
    for i in range(a, b - 1, -1):
        print(i)
"""
 Циклом for вывести ромб
"""
rows = 8
k = 2 * rows - 2
for i in range(0, rows):
    for j in range(0, k):
        print(end=" ")
    k = k - 1
    for j in range(0, i + 1):
        print("* ", end="")
    print("")

k = rows - 2

for i in range(rows, -1, -1):
    for j in range(k, 0, -1):
        print(end=" ")
    k = k + 1
    for j in range(0, i + 1):
        print("* ", end="")
    print("")
"""
Посчитать сумму числового ряда от 0 до 14 включительно.Например, 0 + 1 + 2 + 3 +…+14;
"""
sum = 0
for i in range(14 + 1):
    sum += i
print("sum=", sum)
"""
Перемножить все чётные значения в диапазоне от 0 до 436

"""
result = 1
for a in range(1, 436 + 1):
    if a % 2 == 0:
        result = result * a
print("result=", result)
"""
Факториалом числа n называется число 𝑛!=1∙2∙3∙…∙𝑛. Создайте программу, которая вычисляет факториал введённого
        пользователем числа.(цыкл!)
"""
factorial = 1
n = int(input("введите число"))
for i in range(1, n + 1):
    factorial = factorial * i
print("factorial=", factorial)
"""
Используя вложенные циклы и функции print(‘*’, end=’’), print(‘ ‘, end=’’) и print() выведите на экран
        прямоугольный треугольник.
"""
for i in range(1, 8):
    j = 1
    while j < i:
        print("*", end=" ")
        print(" ", end=" ")
        j += 1
    print()
"""
Напишите программу, которая запрашивает три целых числа a, b и x и выводит True, если x лежит между a и b, иначе
        – False.
"""
a = int(input("введите а"))
b = int(input("введите b"))
x = int(input("введите x"))

if a < x < b or b < x < a:
    print("True")
else:
    print("False")
"""
Даны четыре действительных числа: x1, y1, x2, y2. вычислите расстояние между точкой (x1,y1) и (x2,y2). Считайте
        четыре действительных числа и выведите результат работы этой функции.
   
"""
x1 = float(input("введите х1"))
x2 = float(input("введите х2"))
y1 = float(input("введите y1"))
y2 = float(input("введите y2"))

d = ((x2 - x1) ** 2 - (y2 - y1) ** 2) ** 0.5
print(d)
"""
Пользователь вводит год. Определить он высокосный или нет.
"""
user_year = int(input("введите год"))
if user_year % 4 == 0:
    print("год высокосный")
else:
    print("год не высокосный")
"""
 Пользователь делает вклад в размере N $ сроком на years лет под 11.5% годовых (каждый год размер его вклада
        увеличивается на 11.5%. Эти деньги прибавляются к сумме вклада, и на них в следующем году тоже будут проценты).
        Написать программу , где пользователь вводит аргументы a и years, и посчитать сумму, которая будет на счету
        пользователя через years лет.
   
"""
n = float(input("введите сумму вклада"))
years = int(input("введите целое число лет"))
procent = 0.115

if n and years:
    C = n * (1 + procent) ** years
    print("C=", C)

"""
Заданы две клетки шахматной доски. Если они покрашены в один цвет, то выведите слово YES,
        а если в разные цвета — тоNO. Программа получает на вход четыре числа от 1 до 8 каждое, задающие номер столбца
        и номер строки сначала для первой клетки, потом для второй клетки.

"""
x1 = int(input("введите число от 1 до 8"))
y1 = int(input("введите число от 1 до 8"))
x2 = int(input("введите число от 1 до 8"))
y2 = int(input("введите число от 1 до 8"))
if 0 < (x1 and x2 and y1 and y2) <= 8:
    if (x1 + y1 + x2 + y2) % 2 == 0:
        print('YES')
    else:
        print('NO')
else:
    print("вы ввели некоректные данные")

"""
Задается случайное вещественное число. Определить максимальную цифру этого числа.
"""
x = int(input("Введите число"))
y = 0
for i in str(x):
    i = int(i)
    if i > y:
        y = i
print(y)
